Mirametrix Yocto Platform Guide
===============================
:toc:
:icons:
:iconsdir: ./doc/icons/
:sectnumlevels: 1

== Release Notes

=== Version 0.1.0 (coming soon)

* Generates a default Yocto image for X86 Virtual Machines
* Mirametrix MASE recipe skeleton and dependencies

:numbered:

== Firmware build environment setup

This Yocto firmware generation has been tested on Ubuntu 20.04. You can either
use your Linux machine's tools, or use the
https://github.com/savoirfairelinux/cqfd[cqfd] tool to start the build using the
included Dockerfile. More details are given in the next sections of this
document.

=== SSH keys for Git access

The firmware source code is fetched from several git repositories. Some of them
are public and require no credentials.

The part specific to Mirametrix's product is stored on a private git server,
provided by SFL at the moment.

If you don't already have a set of SSH keys in the `~/.ssh` directory of your
build machine, you can create them by running:

  $ ssh-keygen -t rsa -C <your_email@youremail.com>

Then, connect to our https://g1.sfl.team[Gerrit] server and authenticate using
your provided user and password.

In the top right corner:

* Click on "Settings"
* Go to the "SSH Public Keys" section.
* Add the contents of your public SSH key (with the .pub extension). The default
  SSH public key path should be `~/.ssh/id_rsa.pub`.

=== Git defaults

If you want to fetch the source code from SFL's Git server, add this `Host`
setup snippet to your `~/.ssh/config` file:

```
Host g1.sfl.team
    Port 29419
    User your_username
```

NOTE: Specify the SFL username that was provided to you, it is the same you are
using to login into Gerrit.

== Fetching the source using Repo

We are using https://gerrit.googlesource.com/git-repo[repo] to synchronize the
source code using a manifest (an XML file) which describes all git repositories
required to build a complete firmware. The manifest file is hosted in a git
repository named `yocto-repo-manifest`.

=== Prerequisites

* Install `repo` if not already done.

On Ubuntu 18.04 and previous versions, you can run:

  $ sudo apt-get install repo

Or install it manually by following the instructions at
https://gerrit.googlesource.com/git-repo/

=== Cloning the source repositories

Then, you can initialize your Yocto build repositories using `repo`:

  $ cd my_project_dir/
  $ repo init -u <manifest_repo_url>
  $ repo sync

For instance, with Mirametrix's yocto-build project:

  $ cd my_project_dir/
  $ repo init -u ssh://g1.sfl.team:29419/mirametrix/yocto-repo-manifest
  $ repo sync

NOTE: You can use this init/sync process every time you need to refresh the
source repositories as per the latest manifest definitions.

Once the `repo sync` is completed, you should see a git repository named
`yocto-build`, within which all the Yocto layers were fetched under the
`yocto-build/sources/` sub-directory.

  $ cd yocto-build/

NOTE: The initial build process can take up to 4-5 hours on a lower-end
developer machine and will produce approximately 50GB of data. We recommend
using a powerful, non-virtual build machine if you can.

== Building the firmware manually

This method relies on the manual installation of all the tools and dependencies
required on the host machine (in the next chapter, an alternative method using
Docker and a helper tool is also presented).

=== Prerequisites on Ubuntu

The following packages need to be installed:

  $ sudo apt-get update && apt-get install -y ca-certificates build-essential

  $ sudo apt-get install -y gawk wget git-core diffstat unzip texinfo gcc-multilib \
     build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
     xz-utils debianutils iputils-ping libsdl1.2-dev xterm

=== Building the firmware

==== Building X86 Virtual Machine

Yocto build is started by invoking the following command:
----
$ ./build.sh -v -i mirametrix-image -m mirametrix-x86 --distro mirametrix-distro
----

To build an *image* with `mirametrix-mase` suite, invoke following command instead:
----
$ ./build.sh -v -i mirametrix-image-mase -m mirametrix-x86 --distro mirametrix-distro
----

[NOTE]
--
You can also invoke custom `bitbake` commands using the `--` separator:
----
$ ./build.sh -v -i mirametrix-image -m mirametrix-x86 \
	--distro mirametrix-distro -- bitbake -c clean package_name
----
--

==== Building Renesas R-Car Starter Kit

This requires changing both the `machine` type and target `image` types, Yocto
build is started by invoking the following command:
----
$ ./build.sh -v -i mirametrix-image-renesas -m m3ulcb --distro mirametrix-distro
----

To build a Renesas R-Car *image* with `mirametrix-mase` suite, target image type must be
changed to `mirametrix-image-renesas-mase`, by invoking following command instead:
----
$ ./build.sh -v -i mirametrix-image-renesas-mase -m m3ulcb --distro mirametrix-distro
----

=== Build artifacts

==== X86 Virtual Machine artifacts

After a successful build, the `build/tmp/deploy/images/qemux86-64/` directory
will contain a file named `mirametrix-image-qemux86-64-xxxx.rootfs.wic.vmdk`.
This file is a standard VMDK file usable with most Virtual Machine systems.

==== Renesas R-Car Starter Kit artifacts

Multiple artifacts are created for the R-Car Stater Kit in the
`build/tmp/deploy/images/m3ulcb/` directory. Make sure a file named
`mirametrix-image-renesas-m3ulcb.wic.xz` has been created in this directory.

When building a Renesas R-Car Starter Kit image with `mirametrix-mase` suite,
Yocto generated artifacts are *also* located in `build/tmp/deploy/images/m3ulcb/`
directory, but the with the `-mase` suffix add to all artifacts. Make sure a file
named `mirametrix-image-renesas-mase-m3ulcb.wic.xz` has been created in this directory.

== Building a firmware using cqfd and Docker

`cqfd` is a quick and convenient way to run commands in the current directory,
but within a pre-defined Docker container. Using `cqfd` avoids installing
anything else than Docker and `repo` on your build machine.

NOTE: We recommend using this method as it greatly simplifies the build
configuration management process.

=== Prerequisites

* Install repo and Docker if not already done.

On Ubuntu, please run:

  $ sudo apt-get install docker.io

* Install cqfd:

```
$ git clone https://github.com/savoirfairelinux/cqfd.git
$ cd cqfd
$ sudo make install
```

The project page on https://github.com/savoirfairelinux/cqfd[Github] contains
detailed information on usage and installation.

* Make sure that docker does not require sudo

Use the following commands to add your Linux user account to the `docker` group:

```
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
```

Log out and log back in, so that your group memberships can be re-evaluated.

=== Building the firmware using cqfd

The first step with `cqfd` is to create the build container. For this, use the
`cqfd init` command:

  $ cd yocto-build/
  $ cqfd init

NOTE: The step above is only required once, as once the container image has been
created on your machine, it will become persistent. Further calls to `cqfd init`
will do nothing, unless the container definition (`.cqfd/docker/Dockerfile`) has
changed in the source tree.

You can then start the build using:

  $ cqfd run

=== Create a bootable SD card for the Renesas R-Car Starter Kit

The Yocto image built for the Renesas R-Car Starter Kit, can be flashed on an SD-Card,
then used as boot medium. To create a bootable SD card, insert an SD of at least 2GB
in the build machine. Retrieve the name of the SD card by using the `fdisk` command
(root privileges might be needed):
----
$ fdisk -l
Disk /dev/sdb: 7.53 GiB, 8068792320 bytes, 15759360 sectors
Disk model: MassStorageClass
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xc4339522
----
In this example, there is only one storage device of 8GB connected to the build
machine, which means that `/dev/sdb` is the name of the SD card.

WARNING: Make sure to backup any data on the SD card as the following step will
erase everything on the SD card.

WARNING: Make sure all partitions of the SD card are not-mounted by entering the
following command (root priviledge might be needed). Note that the name of the
SD card block device (`/dev/sdX`) must be replaced by its real value that was
found in the previous step.

  $ umount /dev/sdX*

The following command will flash the Yocto image onto the SD card. Note that the
name of the SD card block device (`/dev/sdX`) must be replaced by its real value
that was found in the previous step.
----
$ xz -dc build/tmp/deploy/images/m3ulcb/mirametrix-image-renesas-m3ulcb.wic.xz \
	| dd of=/dev/sdX conv=fsync
----
or
----
$ xz -dc build/tmp/deploy/images/m3ulcb/mirametrix-image-renesas-mase-m3ulcb.wic.xz \
	| dd of=/dev/sdX conv=fsync
----
NOTE: This step can take up to 5 minutes depending on the write speed of the SD
card.

=== Configure the bootloader on the Renesas R-Car Starter Kit

NOTE: This step only needs to be performed once for each R-Car Starter Kit.

NOTE: The SD card doesn't have to be inserted in the R-Car Starter Kit to
complete this step.

Before booting the SD card image created in the previous step, some
configuration must be done to the R-Car Starter Kit's bootloader. To do so,
connect a USB cable between the Micro-USB connector of the R-Car Starter Kit
(CN12) and another Linux host machine. Once the USB cable is connected, open a
terminal emulator on the Linux host machine such as `picocom` or `minicom`. The
following command will open picocom, note that the name of the device
(`/dev/ttyUSB0`) may change on your machine:

  $ picocom -b 115200 /dev/ttyUSB0

Once the terminal emulator is opened, power up the R-Car Starter Kit using a 5V
power supply and press the POWER push button (SW8). At this point, some boot
messages should be displayed on the terminal emulator opened on the Linux host
machine:

  [    0.000162] NOTICE:  BL2: R-Car M3 Initial Program Loader(CA57)
  [    0.004598] NOTICE:  BL2: Initial Program Loader(Rev.2.0.6)
  [    0.010131] NOTICE:  BL2: PRR is R-Car M3 Ver.3.0
  [    0.014800] NOTICE:  BL2: Board is Starter Kit Rev.3.0
  [    0.019912] NOTICE:  BL2: Boot device is HyperFlash(80MHz)
  [    0.025340] NOTICE:  BL2: LCM state is CM
  [    0.029380] NOTICE:  BL2: AVS setting succeeded. DVFS_SetVID=0x53
  [    0.035368] NOTICE:  BL2: DDR3200(rev.0.40)
  [    0.046414] NOTICE:  BL2: [COLD_BOOT]
  ...

Pressing any key repeatedly will allow you to drop to the bootloader's
interactive shell which prompts you with an `=>`:

  MMC:   sd@ee100000: 0, sd@ee140000: 1
  Loading Environment from MMC... OK
  In:    serial@e6e88000
  Out:   serial@e6e88000
  Err:   serial@e6e88000
  Net:
  Error: ethernet@e6800000 address not set.
  eth-1: ethernet@e6800000
  Hit any key to stop autoboot:  0
  =>

Once the interactive shell of the bootloader is reached, the `bootcmd`, `sdboot`
and `sdbootargs` variables of the bootloader can be configured using the
following commands:

  => setenv bootcmd 'run sdboot'
  => setenv sdboot 'ext4load mmc 0:1 0x48080000 /boot/Image; ext4load mmc 0:1
  0x48000000 /boot/r8a77960-ulcb.dtb; setenv bootargs ${sdbootargs}; booti
  0x48080000 - 0x48000000'
  => setenv sdbootargs 'root=/dev/mmcblk1p1 rw rootwait'
  => saveenv

To make sure the bootloader is properly configured, the following command can be
used to inspect the `bootcmd`, `sdboot` and `sdbootargs` variables:

  => printenv
  ...
  bootcmd=run sdboot
  ...
  sdboot=ext4load mmc 0:1 0x48080000 /boot/Image; ext4load mmc 0:1 0x48000000
    /boot/r8a77960-ulcb.dtb; setenv bootargs ${sdbootargs}; booti 0x48080000 -
    0x48000000
  sdbootargs=root=/dev/mmcblk1p1 rw rootwait
  ...

=== Booting the SD card on the Renesas R-Car Starter Kit

Once the SD card has been prepared with the Yocto image and that the R-Car
Starter Kit bootloader has been properly configured, the SD card is ready to be
booted on the R-Car Starter Kit. Simply insert the SD card in the R-Car Starter
Kit's SD card reader slot and cycle the power using the POWER push button (SW8).
The board should automatically boot to Linux and prompts you with a login
message. Use the `root` username with no password to login:
----
  Mirametrix Distribution 1.0 m3ulcb ttySC0

  m3ulcb login: root
----

== Building an SDK Installer

You can create a Yocto SDK matching your system's configuration using with the
following command:

  $ ./build.sh -v -i mirametrix-image -m mirametrix-x86 --sdk

NOTE: if using cqfd, you will need to prefix this command with `cqfd run`.

When the build command completes, the toolchain installer will be located in
the `tmp/deploy/sdk/` directory under your build directory.

== About this documentation

This documentation uses the AsciiDoc documentation generator. It is a convenient
format that allows using plain-text formatted writing that can later be
converted to various output formats such as HTML and PDF.

In order to generate an HTML version of this documentation, use the following
command (the asciidoc package will need to be installed in your Linux
distribution):

  $ asciidoctor README.adoc

This will result in a README.html file being generated in the current directory.

If you prefer a PDF version of the documentation instead, use the following
command:

  $ asciidoctor-pdf README.adoc
